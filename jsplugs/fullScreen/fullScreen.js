loadCss("jsplugs/fullScreen/fullScreen.css");
$(function(){
    plugs.add("fullScreen",'<i class="xicon-expand-3"> </i>').click(function(){fullScreen()});
})
function fullScreen(var1){
    $("body").toggleClass("full",var1);
    var var2 = $("body").hasClass("full");
    $(".fullScreen i").toggleClass("xicon-expand-3",!var2);
    $(".fullScreen i").toggleClass("xicon-contract-2",!!var2);
    for (var i = 0; i < 4; i++) {
        setTimeout('nowEditor().resize();',100*i);
    };
}
// xicon-collapse